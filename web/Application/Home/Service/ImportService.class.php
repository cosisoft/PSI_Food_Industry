<?php

namespace Home\Service;

use Think\Exception;

require __DIR__ . '/../Common/Excel/PHPExcel/IOFactory.php';

/**
 * PHPExcel文件 Service
 *
 * @author James(张健)
 */
class ImportService extends PSIBaseService {

	/**
	 * 商品导入Service
	 *
	 * @param
	 *        	$params
	 * @return array
	 * @throws \PHPExcel_Exception
	 */
	public function importGoodsFromExcelFile($params) {
		$dataFile = $params["datafile"];
		$ext = $params["ext"];
		$message = "";
		$success = true;
		$result = array(
				"msg" => $message,
				"success" => $success
		);
		if (! $dataFile || ! $ext)
			return $result;
		
		$inputFileType = 'Excel5';
		if ($ext == 'xlsx')
			$inputFileType = 'Excel2007';
			
			// 设置php服务器可用内存，上传较大文件时可能会用到
		ini_set('memory_limit', '1024M');
		ini_set('max_execution_time', 300); // 300 seconds = 5 minutes
		$objReader = \PHPExcel_IOFactory::createReader($inputFileType);
		// 设置只读，可取消类似"3.08E-05"之类自动转换的数据格式，避免写库失败
		$objReader->setReadDataOnly(true);
		try {
			// 载入文件
			$objPHPExcel = $objReader->load($dataFile);
			// 获取表中的第一个工作表
			$currentSheet = $objPHPExcel->getSheet(0);
			// 获取总行数
			$allRow = $currentSheet->getHighestRow();
			
			// 如果没有数据行，直接返回
			if ($allRow < 2)
				return $result;
			
			$ps = new PinyinService();
			$idGen = new IdGenService();
			$bs = new BizlogService();
			$gs = new GoodsService();
			$db = M();
			$units = array(); // 将计量单位缓存，以免频繁访问数据库
			$categories = array(); // 同上
			$params = array(); // 数据参数
			
			$us = new UserService();
			$dataOrg = $us->getLoginUserDataOrg();
			
			$insertSql = "insert into t_goods (id, code, name, spec, category_id, unit_id, sale_price,	py, 
					purchase_price, bar_code, data_org, memo, spec_py) values";
			$dataSql = "('%s', '%s', '%s', '%s', '%s', '%s', %f, '%s', %f, '%s', '%s', '%s', '%s'),";
			/**
			 * 单元格定义
			 * A 商品分类编码
			 * B 商品编码
			 * C 商品名称
			 * D 规格型号
			 * E 计量单位
			 * F 销售单价
			 * G 建议采购单价
			 * H 条形码
			 * I 备注
			 */
			// 从第2行获取数据
			for($currentRow = 2; $currentRow <= $allRow; $currentRow ++) {
				// 数据坐标
				$indexCategory = 'A' . $currentRow;
				$indexCode = 'B' . $currentRow;
				$indexName = 'C' . $currentRow;
				$indexSpec = 'D' . $currentRow;
				$indexUnit = 'E' . $currentRow;
				$indexSalePrice = 'F' . $currentRow;
				$indexPurchasePrice = 'G' . $currentRow;
				$indexBarcode = 'H' . $currentRow;
				$indexMemo = 'I' . $currentRow;
				// 读取到的数据，保存到数组$arr中
				$category = $currentSheet->getCell($indexCategory)->getValue();
				$code = $currentSheet->getCell($indexCode)->getValue();
				$name = $currentSheet->getCell($indexName)->getValue();
				$spec = $currentSheet->getCell($indexSpec)->getValue();
				$unit = $currentSheet->getCell($indexUnit)->getValue();
				$salePrice = $currentSheet->getCell($indexSalePrice)->getValue();
				$purchasePrice = $currentSheet->getCell($indexPurchasePrice)->getValue();
				$barcode = $currentSheet->getCell($indexBarcode)->getValue();
				$memo = $currentSheet->getCell($indexMemo)->getValue();
				
				// 如果为空则直接读取下一条记录
				if (! $category || ! $code || ! $name || ! $unit)
					continue;
				
				$unitId = null;
				$categoryId = null;
				
				if ($units["{$unit}"]) {
					$unitId = $units["{$unit}"];
				} else {
					$sql = "select id, `name` from t_goods_unit where `name` = '%s' ";
					$data = $db->query($sql, $unit);
					if (! $data) {
						// 新增计量单位
						$newUnitParams = array(
								"name" => $unit
						);
						$newUnit = $gs->editUnit($newUnitParams);
						$unitId = $newUnit["id"];
					} else {
						$unitId = $data[0]["id"];
					}
					$units += array(
							"{$unit}" => "{$unitId}"
					);
				}
				
				if ($categories["{$category}"]) {
					$categoryId = $categories["{$category}"];
				} else {
					$sql = "select id, code from t_goods_category where code = '%s' ";
					$data = $db->query($sql, $category);
					if (! $data) {
						// 新增分类
						continue;
					} else {
						$categoryId = $data[0]["id"];
					}
					$categories += array(
							"{$category}" => "{$categoryId}"
					);
				}
				
				// 新增
				// 检查商品编码是否唯一
				$sql = "select 1  from t_goods where code = '%s' ";
				$data = $db->query($sql, $code);
				if ($data) {
					$message .= "商品: 商品编码 = {$code}, 品名 = {$name}, 规格型号 = {$spec} 已存在; \r\n";
					continue;
				}
				
				// 如果录入了条形码，则需要检查条形码是否唯一
				if ($barcode) {
					$sql = "select 1  from t_goods where bar_code = '%s' ";
					$data = $db->query($sql, $barcode);
					if ($data) {
						$message .= "商品: 商品编码 = {$code}, 品名 = {$name}, 规格型号 = {$spec}，条形码 = {$barcode} 已存在;\r\n";
						continue;
					}
				}
				
				$id = $idGen->newId();
				$py = $ps->toPY($name);
				$specPY = $ps->toPY($spec);
				
				$insertSql .= $dataSql;
				// 数据参数加入
				array_push($params, $id, $code, $name, $spec, $categoryId, $unitId, $salePrice, $py, 
						$purchasePrice, $barcode, $dataOrg, $memo, $specPY);
			}
			
			$db->execute(rtrim($insertSql, ','), $params);
			
			$log = "导入方式新增商品;{$dataFile}";
			$bs->insertBizlog($log, "基础数据-商品");
		} catch ( Exception $e ) {
			$success = false;
			$message = $e;
		}
		
		$result = array(
				"msg" => $message,
				"success" => $success
		);
		return $result;
	}

	/**
	 * 获得计量单位Id，如果该计量单位不存在就新建一个
	 *
	 * @param string $unitName
	 *        	计量单位名称
	 */
	private function getUnitId($unitName) {
		$db = M();
		$sql = "select id from t_goods_unit where name = '%s' ";
		$data = $db->query($sql, $unitName);
		if ($data) {
			return $data[0]["id"];
		}
		
		// 计量单位不存在，新建一个
		$idGen = new IdGenService();
		$us = new UserService();
		$dataOrg = $us->getLoginUserDataOrg();
		$companyId = $us->getCompanyId();
		$id = $idGen->newId($db);
		
		$sql = "insert into t_goods_unit(id, name, data_org, company_id)
				values ('%s', '%s', '%s', '%s')";
		$db->execute($sql, $id, $unitName, $dataOrg, $companyId);
		
		return $id;
	}

	/**
	 * 获得商品分类的Id，如果该商品分类不存在就新建一个
	 *
	 * @param string $categoryName
	 *        	商品分类
	 */
	private function getGoodsCategoryId($categoryName) {
		$db = M();
		$sql = "select id from t_goods_category where name = '%s' ";
		$data = $db->query($sql, $categoryName);
		if ($data) {
			return $data[0]["id"];
		}
		
		// 商品分类不存在，新建一个
		$idGen = new IdGenService();
		$us = new UserService();
		$dataOrg = $us->getLoginUserDataOrg();
		$companyId = $us->getCompanyId();
		$id = $idGen->newId($db);
		
		$sql = "insert into t_goods_category(id, code, name, parent_id, full_name, data_org, company_id)
				values ('%s', '%s', '%s', null, '%s', '%s', '%s')";
		$db->execute($sql, $id, $categoryName, $categoryName, $categoryName, $dataOrg, $companyId);
		
		return $id;
	}

	/**
	 * 获得品牌的Id，如果不存在就新建一个
	 *
	 * @param string $brandName        	
	 */
	private function getGoodsBrandId($brandName) {
		$db = M();
		
		$sql = "select id from t_goods_brand where name = '%s' ";
		$data = $db->query($sql, $brandName);
		
		if ($data) {
			return $data[0]["id"];
		}
		
		// 品牌不存在，新建一个
		$idGen = new IdGenService();
		$us = new UserService();
		$dataOrg = $us->getLoginUserDataOrg();
		$companyId = $us->getCompanyId();
		$id = $idGen->newId($db);
		
		$sql = "insert into t_goods_brand(id, name, parent_id, full_name, data_org, company_id)
				values ('%s', '%s', null, '%s', '%s', '%s')";
		$db->execute($sql, $id, $brandName, $brandName, $dataOrg, $companyId);
		
		return $id;
	}

	/**
	 * 导入万里牛商品数据
	 */
	public function importWLNGoodsFromExcelFile($params) {
		$dataFile = $params["datafile"];
		$ext = $params["ext"];
		$message = "";
		$success = true;
		$result = array(
				"msg" => $message,
				"success" => $success
		);
		if (! $dataFile || ! $ext)
			return $result;
		
		$inputFileType = 'Excel5';
		if ($ext == 'xlsx') {
			$inputFileType = 'Excel2007';
		}
		
		// 设置php服务器可用内存，上传较大文件时可能会用到
		ini_set('memory_limit', '1024M');
		ini_set('max_execution_time', 300); // 300 seconds = 5 minutes
		$objReader = \PHPExcel_IOFactory::createReader($inputFileType);
		// 设置只读，可取消类似"3.08E-05"之类自动转换的数据格式，避免写库失败
		$objReader->setReadDataOnly(true);
		try {
			// 载入文件
			$objPHPExcel = $objReader->load($dataFile);
			// 获取表中的第一个工作表
			$currentSheet = $objPHPExcel->getSheet(0);
			// 获取总行数
			$allRow = $currentSheet->getHighestRow();
			
			// 如果没有数据行，直接返回
			if ($allRow < 2)
				return $result;
			
			$ps = new PinyinService();
			$idGen = new IdGenService();
			$bs = new BizlogService();
			$db = M();
			
			$us = new UserService();
			$dataOrg = $us->getLoginUserDataOrg();
			$companyId = $us->getCompanyId();
			
			/**
			 * 单元格定义
			 * A 商品编码
			 * B 商品名称
			 * C 规格编码 -- 有规格型号的时候，相当于是商品编码
			 * D 规格值1
			 * E 规格值2
			 * F 条码
			 * G 货号 -- 不导入，忽略
			 * H 重量
			 * I 长 -- 不导入，忽略
			 * J 宽 -- 不导入，忽略
			 * K 高 -- 不导入，忽略
			 * L 体积 -- 不导入，忽略
			 * M 期初库存 -- 不导入，忽略
			 * N 期初成本均价 -- 不导入，忽略
			 * O 货位编码 -- 不导入，忽略
			 * P 分类
			 * Q 品牌
			 * R 标准售价
			 * S 批发价 -- 不导入，忽略
			 * T 参考进价
			 * U 吊牌价 -- 不导入，忽略
			 * V 单位
			 * X 图片 -- 不导入，忽略
			 * W 图片 -- 不导入，忽略
			 * Y 图片 -- 不导入，忽略
			 * Z 图片 -- 不导入，忽略
			 * AA 备注
			 * AB 生产商
			 * AC 保质期
			 */
			
			// 从第2行获取数据
			$lastName = "";
			for($currentRow = 2; $currentRow <= $allRow; $currentRow ++) {
				// 数据坐标
				$indexCategory = 'P' . $currentRow;
				$indexCode = 'A' . $currentRow;
				$indexCode2 = "C" . $currentRow;
				$indexName = 'B' . $currentRow;
				$indexSpec1 = 'D' . $currentRow;
				$indexSpec2 = 'E' . $currentRow;
				$indexUnit = 'V' . $currentRow;
				$indexSalePrice = 'R' . $currentRow;
				$indexPurchasePrice = 'T' . $currentRow;
				$indexBarcode = 'F' . $currentRow;
				$indexMemo = 'AA' . $currentRow;
				$indexManufacturer = "AB" . $currentRow;
				$indexExpiration = "AC" . $currentRow;
				$indexWeight = "H" . $currentRow;
				$indexBrand = "Q" . $currentRow;
				
				$categoryName = $currentSheet->getCell($indexCategory)->getValue();
				$code = $currentSheet->getCell($indexCode)->getValue();
				$code2 = $currentSheet->getCell($indexCode2)->getValue();
				$name = $currentSheet->getCell($indexName)->getValue();
				if (! $name) {
					$name = $lastName;
				} else {
					$lastName = $name;
				}
				$spec1 = $currentSheet->getCell($indexSpec1)->getValue();
				$spec2 = $currentSheet->getCell($indexSpec2)->getValue();
				$unitName = $currentSheet->getCell($indexUnit)->getValue();
				$salePrice = $currentSheet->getCell($indexSalePrice)->getValue();
				$purchasePrice = $currentSheet->getCell($indexPurchasePrice)->getValue();
				$barcode = $currentSheet->getCell($indexBarcode)->getValue();
				$memo = $currentSheet->getCell($indexMemo)->getValue();
				$brandName = $currentSheet->getCell($indexBrand)->getValue();
				$manufacturer = $currentSheet->getCell($indexManufacturer)->getValue();
				$weight = $currentSheet->getCell($indexWeight)->getValue();
				$expiration = $currentSheet->getCell($indexExpiration)->getValue();
				
				$spec = $spec1 . " " . $spec2;
				if ($code2) {
					$code = $code2;
				}
				
				// 检查商品是否已经导入过了
				$sql = "select count(*) as cnt from t_goods where code = '%s' ";
				$data = $db->query($sql, $code);
				$cnt = $data[0]["cnt"];
				if ($cnt > 0) {
					continue;
				}
				
				$unitId = $this->getUnitId($unitName);
				$categoryId = $this->getGoodsCategoryId($categoryName);
				$branId = $this->getGoodsBrandId($brandName);
				
				$id = $idGen->newId();
				$py = $ps->toPY($name);
				$specPY = $ps->toPY($spec);
				$sql = "insert into t_goods (id, code, name, spec, category_id, unit_id, sale_price,
						py, purchase_price, bar_code, memo, data_org, company_id, spec_py, brand_id,
						expiration, manufacturer, weight)
					values ('%s', '%s', '%s', '%s', '%s', '%s', %f, '%s', %f, '%s', '%s', '%s', '%s', '%s',
						'%s', %d, '%s', %f)";
				$db->execute($sql, $id, $code, $name, $spec, $categoryId, $unitId, $salePrice, $py, 
						$purchasePrice, $barcode, $memo, $dataOrg, $companyId, $specPY, $branId, 
						$expiration, $manufacturer, $weight);
			}
			
			$log = "导入万里牛商品数据";
			$bs->insertBizlog($log, "基础数据-商品");
		} catch ( Exception $e ) {
			$success = false;
			$message = $e;
		}
		
		$result = array(
				"msg" => $message,
				"success" => $success
		);
		return $result;
	}

	/**
	 * 获得默认客户分类的id，如果没有就新建一个
	 */
	private function getDefaultCustomerCategoryId() {
		$db = M();
		
		$categoryName = "默认分类";
		$sql = "select id from t_customer_category where name = '%s' ";
		$data = $db->query($sql, $categoryName);
		if ($data) {
			return $data[0]["id"];
		}
		
		// 新建一个默认分类
		$idGen = new IdGenService();
		$id = $idGen->newId($db);
		
		$us = new UserService();
		$dataOrg = $us->getLoginUserDataOrg();
		$companyId = $us->getCompanyId();
		
		$sql = "insert into t_customer_category(id, code, name, parent_id, data_org, company_id, ps_id)
				values ('%s', '%s', '%s', null, '%s', '%s', null)";
		$db->execute($sql, $id, $categoryName, $categoryName, $dataOrg, $companyId);
		
		return $id;
	}

	/**
	 * 获得默认供应商分类的id，如果没有就新建一个
	 */
	private function getDefaultSupplierCategoryId() {
		$db = M();
		
		$categoryName = "默认分类";
		$sql = "select id from t_supplier_category where name = '%s' ";
		$data = $db->query($sql, $categoryName);
		if ($data) {
			return $data[0]["id"];
		}
		
		// 新建一个默认分类
		$idGen = new IdGenService();
		$id = $idGen->newId($db);
		
		$us = new UserService();
		$dataOrg = $us->getLoginUserDataOrg();
		$companyId = $us->getCompanyId();
		
		$sql = "insert into t_supplier_category(id, code, name, parent_id, data_org, company_id)
				values ('%s', '%s', '%s', null, '%s', '%s')";
		$db->execute($sql, $id, $categoryName, $categoryName, $dataOrg, $companyId);
		
		return $id;
	}

	/**
	 * 如果字符串以'开头，则把它去掉
	 *
	 * @param string $s        	
	 */
	private function trimPrex($s) {
		return ltrim($s, "'");
	}

	/**
	 * 导入万里牛客户资料
	 */
	public function importWLNCustomerFromExcelFile($params) {
		$dataFile = $params["datafile"];
		$ext = $params["ext"];
		$message = "";
		$success = true;
		$result = array(
				"msg" => $message,
				"success" => $success
		);
		
		if (! $dataFile || ! $ext) {
			return $result;
		}
		
		// 设置php服务器可用内存，上传较大文件时可能会用到
		ini_set('memory_limit', '1024M');
		// Deal with the Fatal error: Maximum execution time of 30 seconds exceeded
		ini_set('max_execution_time', 3000); // 3000 seconds = 50 minutes
		
		if ($ext == 'xlsx') {
			$objReader = new \PHPExcel_Reader_Excel2007();
		} else if ($ext == 'xls') {
			$objReader = new \PHPExcel_Reader_Excel5();
		} else if ($ext == 'csv') {
			$objReader = new \PHPExcel_Reader_CSV();
			
			// 默认输入字符集
			$objReader->setInputEncoding('GBK');
			
			// 默认的分隔符
			$objReader->setDelimiter(',');
		}
		
		// 设置只读，可取消类似"3.08E-05"之类自动转换的数据格式，避免写库失败
		$objReader->setReadDataOnly(true);
		
		try {
			// 载入文件
			$objPHPExcel = $objReader->load($dataFile);
			// 获取表中的第一个工作表
			$currentSheet = $objPHPExcel->getSheet(0);
			// 获取总行数
			$allRow = $currentSheet->getHighestRow();
			
			// 如果没有数据行，直接返回
			if ($allRow < 2)
				return $result;
			
			$ps = new PinyinService();
			$idGen = new IdGenService();
			$bs = new BizlogService();
			$db = M();
			
			$us = new UserService();
			$dataOrg = $us->getLoginUserDataOrg();
			$companyId = $us->getCompanyId();
			
			/**
			 * 单元格定义
			 * A 客户编码
			 * B 客户名称
			 * C 淘宝昵称
			 * D 应收款 -- 不导入，忽略
			 * E 省份
			 * F 市
			 * G 区
			 * H 地址
			 * I 邮编
			 * J 固话
			 * K 手机
			 * L email
			 * M QQ号
			 * N 备注
			 */
			
			// 从第2行获取数据
			for($currentRow = 2; $currentRow <= $allRow; $currentRow ++) {
				// 数据坐标
				$indexCode = 'A' . $currentRow;
				$indexName = 'B' . $currentRow;
				$indexNickName = 'C' . $currentRow;
				$indexProvince = 'E' . $currentRow;
				$indexCity = 'F' . $currentRow;
				$indexDisctrict = 'G' . $currentRow;
				$indexAddress = 'H' . $currentRow;
				$indexPostcode = 'I' . $currentRow;
				$indexNote = 'N' . $currentRow;
				$indexTel = "J" . $currentRow;
				$indexMobile = "K" . $currentRow;
				$indexQQ = "M" . $currentRow;
				$indexEmail = "L" . $currentRow;
				
				$code = $currentSheet->getCell($indexCode)->getValue();
				$code = $this->trimPrex($code);
				$name = $currentSheet->getCell($indexName)->getValue();
				$tel = $currentSheet->getCell($indexTel)->getValue();
				$tel = $this->trimPrex($tel);
				$qq = $currentSheet->getCell($indexQQ)->getValue();
				$mobile = $currentSheet->getCell($indexMobile)->getValue();
				$mobile = $this->trimPrex($mobile);
				$address = $currentSheet->getCell($indexAddress)->getValue();
				$email = $currentSheet->getCell($indexEmail)->getValue();
				$note = $currentSheet->getCell($indexNote)->getValue();
				$province = $currentSheet->getCell($indexProvince)->getValue();
				$city = $currentSheet->getCell($indexCity)->getValue();
				$district = $currentSheet->getCell($indexDisctrict)->getValue();
				$postcode = $currentSheet->getCell($indexPostcode)->getValue();
				$postcode = $this->trimPrex($postcode);
				$nickname = $currentSheet->getCell($indexNickName)->getValue();
				
				// 如果为空则直接读取下一条记录
				if (! $code || ! $name) {
					continue;
				}
				
				// 检查客户资料是否已经存在
				$sql = "select count(*) as cnt from t_customer where code = '%s' ";
				$data = $db->query($sql, $code);
				$cnt = $data[0]["cnt"];
				if ($cnt > 0) {
					continue;
				}
				
				$categoryId = $this->getDefaultCustomerCategoryId();
				
				$id = $idGen->newId();
				$py = $ps->toPY($name);
				
				$sql = "insert into t_customer(id, code, name, category_id, tel01, 
							mobile01, email01, qq01, province, city, 
							district, postcode, taobao_nickname, address, note, py)
						values ('%s', '%s', '%s', '%s', '%s', 
							'%s', '%s', '%s', '%s', '%s',
							'%s', '%s', '%s', '%s', '%s', '%s')";
				$db->execute($sql, $id, $code, $name, $categoryId, $tel, $mobile, $email, $qq, 
						$province, $city, $district, $postcode, $nickname, $address, $note, $py);
			} // for
			
			$log = "导入方式万里牛客户数据";
			$bs->insertBizlog($log, "客户关系-客户资料");
		} catch ( Exception $e ) {
			$success = false;
			$message = $e;
		}
		
		return array(
				"msg" => $message,
				"success" => $success
		);
	}

	/**
	 * 客户导入Service
	 *
	 * @param
	 *        	$params
	 * @return array
	 * @throws \PHPExcel_Exception
	 */
	public function importCustomerFromExcelFile($params) {
		$dataFile = $params["datafile"];
		$ext = $params["ext"];
		$message = "";
		$success = true;
		$result = array(
				"msg" => $message,
				"success" => $success
		);
		
		if (! $dataFile || ! $ext)
			return $result;
		
		$inputFileType = 'Excel5';
		if ($ext == 'xlsx')
			$inputFileType = 'Excel2007';
			
			// 设置php服务器可用内存，上传较大文件时可能会用到
		ini_set('memory_limit', '1024M');
		// Deal with the Fatal error: Maximum execution time of 30 seconds exceeded
		ini_set('max_execution_time', 300); // 300 seconds = 5 minutes
		$objReader = \PHPExcel_IOFactory::createReader($inputFileType);
		// 设置只读，可取消类似"3.08E-05"之类自动转换的数据格式，避免写库失败
		$objReader->setReadDataOnly(true);
		
		try {
			// 载入文件
			$objPHPExcel = $objReader->load($dataFile);
			// 获取表中的第一个工作表
			$currentSheet = $objPHPExcel->getSheet(0);
			// 获取总行数
			$allRow = $currentSheet->getHighestRow();
			
			// 如果没有数据行，直接返回
			if ($allRow < 2)
				return $result;
			
			$ps = new PinyinService();
			$idGen = new IdGenService();
			$bs = new BizlogService();
			$db = M();
			$categories = array(); // 同上
			$params = array(); // 数据参数
			
			$us = new UserService();
			$dataOrg = $us->getLoginUserDataOrg();
			$companyId = $us->getCompanyId();
			
			$insertSql = "
				insert into t_customer (id, category_id, code, name, py,
					contact01, qq01, tel01, mobile01, contact02, qq02, tel02, mobile02, address,
					address_shipping, address_receipt,
					bank_name, bank_account, tax_number, fax, note, data_org)
				values('%s', '%s', '%s', '%s', '%s',
					'%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s',
					'%s', '%s',
					'%s', '%s', '%s', '%s', '%s', '%s')";
			/**
			 * 单元格定义
			 * A category 客户分类编码
			 * B code 客户编码
			 * C name 客户名称 -- py 客户名称的拼音字头
			 * D contact01 联系人
			 * E tel01 联系人固话
			 * F qq01 联系人QQ号
			 * G mobile01 联系人手机
			 * H contact02 备用联系人
			 * I tel02 备用联系人固话
			 * J qq02 备用联系人QQ号
			 * K mobile02 备用联系人手机
			 * L address 地址
			 * M init_receivables 期初应收账款
			 * N init_receivables_dt 期初应收账款截止日期
			 * O address_shipping 发货地址
			 * P address_receipt 收货地址
			 * Q bank_name 开户行
			 * R bank_account 开户行账号
			 * S tax_number 税号
			 * T fax 传真
			 * U note 备注
			 */
			// 从第2行获取数据
			for($currentRow = 2; $currentRow <= $allRow; $currentRow ++) {
				// 数据坐标
				$indexCategory = 'A' . $currentRow;
				$indexCode = 'B' . $currentRow;
				$indexName = 'C' . $currentRow;
				$indexContact01 = 'D' . $currentRow;
				$indexTel01 = 'E' . $currentRow;
				$indexQQ01 = 'F' . $currentRow;
				$indexMobile01 = 'G' . $currentRow;
				$indexContact02 = 'H' . $currentRow;
				$indexTel02 = 'I' . $currentRow;
				$indexQQ02 = 'J' . $currentRow;
				$indexMobile02 = 'K' . $currentRow;
				$indexAddress = 'L' . $currentRow;
				$indexInitReceivables = 'M' . $currentRow;
				$indexInitReceivablesDt = 'N' . $currentRow;
				$indexAddressShipping = 'O' . $currentRow;
				$indexAddressReceipt = 'P' . $currentRow;
				$indexBankName = 'Q' . $currentRow;
				$indexBankAccount = 'R' . $currentRow;
				$indexTaxNumber = 'S' . $currentRow;
				$indexFax = 'T' . $currentRow;
				$indexNote = 'U' . $currentRow;
				// 读取到的数据，保存到数组$arr中
				$category = $currentSheet->getCell($indexCategory)->getValue();
				$code = $currentSheet->getCell($indexCode)->getValue();
				$name = $currentSheet->getCell($indexName)->getValue();
				$contact01 = $currentSheet->getCell($indexContact01)->getValue();
				$tel01 = $currentSheet->getCell($indexTel01)->getValue();
				$qq01 = $currentSheet->getCell($indexQQ01)->getValue();
				$mobile01 = $currentSheet->getCell($indexMobile01)->getValue();
				$contact02 = $currentSheet->getCell($indexContact02)->getValue();
				$tel02 = $currentSheet->getCell($indexTel02)->getValue();
				$qq02 = $currentSheet->getCell($indexQQ02)->getValue();
				$mobile02 = $currentSheet->getCell($indexMobile02)->getValue();
				$address = $currentSheet->getCell($indexAddress)->getValue();
				$initReceivables = $currentSheet->getCell($indexInitReceivables)->getValue();
				$initRDTValue = $currentSheet->getCell($indexInitReceivablesDt)->getValue();
				$initReceivablesDT = null;
				if ($initRDTValue) {
					$intRDTSeconds = intval(($initRDTValue - 25569) * 3600 * 24); // 转换成1970年以来的秒数
					$initReceivablesDT = gmdate('Y-m-d', $intRDTSeconds);
					if ($initReceivablesDT == "1970-01-01") {
						$initReceivablesDT = null;
					}
				}
				
				; // 格式化日期
				$addressShipping = $currentSheet->getCell($indexAddressShipping)->getValue();
				$addressReceipt = $currentSheet->getCell($indexAddressReceipt)->getValue();
				$bankName = $currentSheet->getCell($indexBankName)->getValue();
				$bankAccount = $currentSheet->getCell($indexBankAccount)->getValue();
				$taxNumber = $currentSheet->getCell($indexTaxNumber)->getValue();
				$fax = $currentSheet->getCell($indexFax)->getValue();
				$note = $currentSheet->getCell($indexNote)->getValue();
				
				// 如果为空则直接读取下一条记录
				if (! $category || ! $code || ! $name)
					continue;
				
				$categoryId = null;
				
				if ($categories["{$category}"]) {
					$categoryId = $categories["{$category}"];
				} else {
					$sql = "select id, code from t_customer_category where code = '%s' ";
					$data = $db->query($sql, $category);
					if (! $data) {
						// 新增分类
						continue;
					} else {
						$categoryId = $data[0]["id"];
					}
					$categories += array(
							"{$category}" => "{$categoryId}"
					);
				}
				
				// 新增
				// 检查商品编码是否唯一
				$sql = "select 1 from t_customer where code = '%s' ";
				$data = $db->query($sql, $code);
				if ($data) {
					$message .= "编码为 [{$code}] 的客户已经存在; \r\n";
					continue;
				}
				
				$id = $idGen->newId();
				$py = $ps->toPY($name);
				
				$db->execute($insertSql, $id, $categoryId, $code, $name, $py, $contact01, $qq01, 
						$tel01, $mobile01, $contact02, $qq02, $tel02, $mobile02, $address, 
						$addressShipping, $addressReceipt, $bankName, $bankAccount, $taxNumber, $fax, 
						$note, $dataOrg);
				
				// 处理应收账款
				$initReceivables = floatval($initReceivables);
				
				if ($initReceivables && $initReceivablesDT && $this->dateIsValid($initReceivablesDT)) {
					$sql = "select count(*) as cnt
					from t_receivables_detail
					where ca_id = '%s' and ca_type = 'customer' and ref_type <> '应收账款期初建账' 
							and company_id = '%s' ";
					$data = $db->query($sql, $id, $companyId);
					$cnt = $data[0]["cnt"];
					if ($cnt > 0) {
						// 已经有应收业务发生，就不再更改期初数据
						continue;
					}
					
					$sql = "update t_customer
							set init_receivables = %f, init_receivables_dt = '%s'
							where id = '%s' ";
					$db->execute($sql, $initReceivables, $initReceivablesDT, $id);
					
					// 应收明细账
					$sql = "select id from t_receivables_detail
							where ca_id = '%s' and ca_type = 'customer' and ref_type = '应收账款期初建账' 
							and company_id = '%s' ";
					$data = $db->query($sql, $id, $companyId);
					if ($data) {
						$rvId = $data[0]["id"];
						$sql = "update t_receivables_detail
								set rv_money = %f, act_money = 0, balance_money = %f, biz_date ='%s', 
									date_created = now()
								where id = '%s' ";
						$db->execute($sql, $initReceivables, $initReceivables, $initReceivablesDT, 
								$rvId);
					} else {
						$idGen = new IdGenService();
						$rvId = $idGen->newId();
						$sql = "insert into t_receivables_detail (id, rv_money, act_money, balance_money,
						biz_date, date_created, ca_id, ca_type, ref_number, ref_type, company_id)
						values ('%s', %f, 0, %f, '%s', now(), '%s', 'customer', '%s', '应收账款期初建账', '%s') ";
						$db->execute($sql, $rvId, $initReceivables, $initReceivables, 
								$initReceivablesDT, $id, $id, $companyId);
					}
					
					// 应收总账
					$sql = "select id from t_receivables 
							where ca_id = '%s' and ca_type = 'customer' 
								and company_id = '%s' ";
					$data = $db->query($sql, $id, $companyId);
					if ($data) {
						$rvId = $data[0]["id"];
						$sql = "update t_receivables
							set rv_money = %f, act_money = 0, balance_money = %f
							where id = '%s' ";
						$db->execute($sql, $initReceivables, $initReceivables, $rvId);
					} else {
						$idGen = new IdGenService();
						$rvId = $idGen->newId();
						$sql = "insert into t_receivables (id, rv_money, act_money, balance_money,
								ca_id, ca_type, company_id) 
								values ('%s', %f, 0, %f, '%s', 'customer', '%s')";
						$db->execute($sql, $rvId, $initReceivables, $initReceivables, $id, 
								$companyId);
					}
				}
			} // for
			
			$log = "导入方式新增客户";
			$bs->insertBizlog($log, "客户关系-客户资料");
		} catch ( Exception $e ) {
			$success = false;
			$message = $e;
		}
		
		return array(
				"msg" => $message,
				"success" => $success
		);
	}

	/**
	 * 导入万里牛供应商资料
	 */
	public function importWLNSupplierFromExcelFile($params) {
		$dataFile = $params["datafile"];
		$ext = $params["ext"];
		$message = "";
		$success = true;
		$result = array(
				"msg" => $message,
				"success" => $success
		);
		
		if (! $dataFile || ! $ext) {
			return $result;
		}
		
		// 设置php服务器可用内存，上传较大文件时可能会用到
		ini_set('memory_limit', '1024M');
		// Deal with the Fatal error: Maximum execution time of 30 seconds exceeded
		ini_set('max_execution_time', 3000); // 3000 seconds = 50 minutes
		
		if ($ext == 'xlsx') {
			$objReader = new \PHPExcel_Reader_Excel2007();
		} else if ($ext == 'xls') {
			$objReader = new \PHPExcel_Reader_Excel5();
		} else if ($ext == 'csv') {
			$objReader = new \PHPExcel_Reader_CSV();
			
			// 默认输入字符集
			$objReader->setInputEncoding('GBK');
			
			// 默认的分隔符
			$objReader->setDelimiter(',');
		}
		
		// 设置只读，可取消类似"3.08E-05"之类自动转换的数据格式，避免写库失败
		$objReader->setReadDataOnly(true);
		
		try {
			// 载入文件
			$objPHPExcel = $objReader->load($dataFile);
			// 获取表中的第一个工作表
			$currentSheet = $objPHPExcel->getSheet(0);
			// 获取总行数
			$allRow = $currentSheet->getHighestRow();
			
			// 如果没有数据行，直接返回
			if ($allRow < 2)
				return $result;
			
			$ps = new PinyinService();
			$idGen = new IdGenService();
			$bs = new BizlogService();
			$db = M();
			
			$us = new UserService();
			$dataOrg = $us->getLoginUserDataOrg();
			$companyId = $us->getCompanyId();
			
			/**
			 * 单元格定义
			 * A 供应商编码
			 * B 供应商名称
			 * C 网站网址 -- 不导入，忽略
			 * D 应付款 -- 不导入，忽略
			 * E 省份
			 * F 市
			 * G 区
			 * H 地址
			 * I 邮编
			 * J 联系人
			 * K 固话
			 * L 手机
			 * M email
			 * N 税号
			 * O 传真
			 * P 开户行及账号
			 * Q 备注
			 */
			
			// 从第2行获取数据
			for($currentRow = 2; $currentRow <= $allRow; $currentRow ++) {
				// 数据坐标
				$indexCode = 'A' . $currentRow;
				$indexName = 'B' . $currentRow;
				$indexProvince = 'E' . $currentRow;
				$indexCity = 'F' . $currentRow;
				$indexDisctrict = 'G' . $currentRow;
				$indexAddress = 'H' . $currentRow;
				$indexPostcode = 'I' . $currentRow;
				$indexNote = 'Q' . $currentRow;
				$indexTel = "K" . $currentRow;
				$indexMobile = "L" . $currentRow;
				$indexEmail = "M" . $currentRow;
				$indexContact = "J" . $currentRow;
				
				$code = $currentSheet->getCell($indexCode)->getValue();
				$code = $this->trimPrex($code);
				$name = $currentSheet->getCell($indexName)->getValue();
				$tel = $currentSheet->getCell($indexTel)->getValue();
				$tel = $this->trimPrex($tel);
				$mobile = $currentSheet->getCell($indexMobile)->getValue();
				$mobile = $this->trimPrex($mobile);
				$address = $currentSheet->getCell($indexAddress)->getValue();
				$email = $currentSheet->getCell($indexEmail)->getValue();
				$note = $currentSheet->getCell($indexNote)->getValue();
				$province = $currentSheet->getCell($indexProvince)->getValue();
				$city = $currentSheet->getCell($indexCity)->getValue();
				$district = $currentSheet->getCell($indexDisctrict)->getValue();
				$postcode = $currentSheet->getCell($indexPostcode)->getValue();
				$postcode = $this->trimPrex($postcode);
				$contact = $currentSheet->getCell($indexContact)->getValue();
				
				// 如果为空则直接读取下一条记录
				if (! $code || ! $name) {
					continue;
				}
				
				// 检查客户资料是否已经存在
				$sql = "select count(*) as cnt from t_supplier where code = '%s' ";
				$data = $db->query($sql, $code);
				$cnt = $data[0]["cnt"];
				if ($cnt > 0) {
					continue;
				}
				
				$categoryId = $this->getDefaultSupplierCategoryId();
				
				$id = $idGen->newId();
				$py = $ps->toPY($name);
				
				$sql = "insert into t_supplier(id, code, name, category_id, tel01,
							mobile01, email01, contact01, province, city,
							district, postcode, address, note, py)
						values ('%s', '%s', '%s', '%s', '%s',
							'%s', '%s', '%s', '%s', '%s',
							'%s', '%s', '%s', '%s', '%s')";
				$db->execute($sql, $id, $code, $name, $categoryId, $tel, $mobile, $email, $contact, 
						$province, $city, $district, $postcode, $address, $note, $py);
			} // for
			
			$log = "导入方式万里牛供应商档案";
			$bs->insertBizlog($log, "基础数据-供应商档案");
		} catch ( Exception $e ) {
			$success = false;
			$message = $e;
		}
		
		return array(
				"msg" => $message,
				"success" => $success
		);
	}

	/**
	 * 通过导入万里牛库存数据建账
	 */
	public function importWLNInitInventoryFromExcelFile($params) {
		$dataFile = $params["datafile"];
		$ext = $params["ext"];
		$message = "";
		$success = true;
		$result = array(
				"msg" => $message,
				"success" => $success
		);
		
		if (! $dataFile || ! $ext) {
			return $result;
		}
		
		// 设置php服务器可用内存，上传较大文件时可能会用到
		ini_set('memory_limit', '1024M');
		// Deal with the Fatal error: Maximum execution time of 30 seconds exceeded
		ini_set('max_execution_time', 3000); // 3000 seconds = 50 minutes
		
		if ($ext == 'xlsx') {
			$objReader = new \PHPExcel_Reader_Excel2007();
		} else if ($ext == 'xls') {
			$objReader = new \PHPExcel_Reader_Excel5();
		} else if ($ext == 'csv') {
			$objReader = new \PHPExcel_Reader_CSV();
			
			// 默认输入字符集
			$objReader->setInputEncoding('GBK');
			
			// 默认的分隔符
			$objReader->setDelimiter(',');
		}
		
		// 设置只读，可取消类似"3.08E-05"之类自动转换的数据格式，避免写库失败
		$objReader->setReadDataOnly(true);
		
		try {
			// 载入文件
			$objPHPExcel = $objReader->load($dataFile);
			// 获取表中的第一个工作表
			$currentSheet = $objPHPExcel->getSheet(0);
			// 获取总行数
			$allRow = $currentSheet->getHighestRow();
			
			// 如果没有数据行，直接返回
			if ($allRow < 2)
				return $result;
			
			$idGen = new IdGenService();
			$bs = new BizlogService();
			$db = M();
			
			$us = new UserService();
			$dataOrg = $us->getLoginUserDataOrg();
			$companyId = $us->getCompanyId();
			
			$ps = new PinyinService();
			
			/**
			 * 单元格定义
			 * A 商品条码 -- 不导入，忽略
			 * B 商品编码
			 * C 商品名称 -- 不导入，忽略
			 * D 规格名称 -- 不导入，忽略
			 * E 品牌 -- 不导入，忽略
			 * F 分类 -- 不导入，忽略
			 * G 仓库名称
			 * H 实际库存
			 * I 可用库存 -- 不导入，忽略
			 * J 在途库存 -- 不导入，忽略
			 * K 成本均价
			 * L 成本总价
			 */
			$warehouseLabel = $currentSheet->getCell("G1")->getValue();
			if ($warehouseLabel != "仓库名称") {
				return $this->bad("导入的文件中没有仓库名称字段");
			}
			
			// 从第2行获取数据
			for($currentRow = 2; $currentRow <= $allRow; $currentRow ++) {
				// 数据坐标
				$indexCode = "B" . $currentRow;
				$indexWarehouse = "G" . $currentRow;
				$indexCount = "H" . $currentRow;
				$indexPrice = "K" . $currentRow;
				$indexMoney = "L" . $currentRow;
				
				$code = $currentSheet->getCell($indexCode)->getValue();
				$code = $this->trimPrex($code);
				$warehouse = $currentSheet->getCell($indexWarehouse)->getValue();
				$goodsCount = $currentSheet->getCell($indexCount)->getValue();
				if ($goodsCount <= 0) {
					// 数量小于或等于0，就忽略该记录
					continue;
				}
				
				$goodsMoney = $currentSheet->getCell($indexMoney)->getValue();
				
				$goodsPrice = $goodsMoney / $goodsCount;
				
				// 如果为空则直接读取下一条记录
				if (! $code || ! $warehouse) {
					continue;
				}
				
				// 检查商品是否已经存在，如果不存在就忽略
				$sql = "select id from t_goods where code = '%s' ";
				$data = $db->query($sql, $code);
				if (! $data) {
					continue;
				}
				$goodsId = $data[0]["id"];
				
				// 检查仓库是否存在，如果不存在就新建一个
				$sql = "select id, inited from t_warehouse where name = '%s' ";
				$data = $db->query($sql, $warehouse);
				$warehouseId = null;
				if ($data) {
					$warehouseId = $data[0]["id"];
					$inited = $data[0]["inited"];
					if ($inited == 1) {
						// 仓库已经完成建账，忽略该笔导入数据
						continue;
					}
				} else {
					// 仓库不存在，新建一个
					$warehouseId = $idGen->newId($db);
					$py = $ps->toPY($warehouse);
					$sql = "insert into t_warehouse(id, code, name, inited, py, 
								data_org, company_id)
							values ('%s', '%s', '%s', 0, '%s', '%s', '%s')";
					$db->execute($sql, $warehouseId, $warehouse, $warehouse, $py, $dataOrg, 
							$companyId);
				}
				
				// 库存总账
				$sql = "select id from t_inventory_lot 
						where warehouse_id = '%s' and goods_id = '%s'
							and begin_dt = '1970-01-01' and expiration = 0
						";
				$data = $db->query($sql, $warehouseId, $goodsId);
				if (! $data) {
					$sql = "insert into t_inventory_lot (warehouse_id, goods_id, in_count, in_price,
						in_money, balance_count, balance_price, balance_money, data_org,
							begin_dt, expiration, end_dt)
						values ('%s', '%s', %d, %f, %f, %d, %f, %f, '%s', '1970-01-01', 0, '1970-01-01') ";
					$db->execute($sql, $warehouseId, $goodsId, $goodsCount, $goodsPrice, 
							$goodsMoney, $goodsCount, $goodsPrice, $goodsMoney, $dataOrg);
				} else {
					$id = $data[0]["id"];
					$sql = "update t_inventory_lot
						set in_count = %d, in_price = %f, in_money = %f,
						balance_count = %d, balance_price = %f, balance_money = %f
						where id = %d ";
					$db->execute($sql, $goodsCount, $goodsPrice, $goodsMoney, $goodsCount, 
							$goodsPrice, $goodsMoney, $id);
				}
				
				// 库存明细账
				$sql = "select id from t_inventory_detail_lot
					where warehouse_id = '%s' and goods_id = '%s' and ref_type = '库存建账' 
						and begin_dt = '1970-01-01' and expiration = 0
						";
				$data = $db->query($sql, $warehouseId, $goodsId);
				if (! $data) {
					$sql = "insert into t_inventory_detail_lot (warehouse_id, goods_id,  in_count, in_price,
						in_money, balance_count, balance_price, balance_money,
						biz_date, biz_user_id, date_created,  ref_number, ref_type, data_org,
							begin_dt, expiration, end_dt)
						values ('%s', '%s', %d, %f, %f, %d, %f, %f, curdate(), '%s', now(), 
							'', '库存建账', '%s', '1970-01-01', 0, '1970-01-01')";
					$db->execute($sql, $warehouseId, $goodsId, $goodsCount, $goodsPrice, 
							$goodsMoney, $goodsCount, $goodsPrice, $goodsMoney, 
							$us->getLoginUserId(), $dataOrg);
				} else {
					$id = $data[0]["id"];
					$sql = "update t_inventory_detail_lot
						set in_count = %d, in_price = %f, in_money = %f,
						balance_count = %d, balance_price = %f, balance_money = %f,
						biz_date = curdate()
						where id = %d ";
					$db->execute($sql, $goodsCount, $goodsPrice, $goodsMoney, $goodsCount, 
							$goodsPrice, $goodsMoney, $id);
				}
			} // for
			
			$log = "通过导入万里牛库存数据库存建账";
			$bs->insertBizlog($log, "库存建账");
		} catch ( Exception $e ) {
			$success = false;
			$message = $e;
		}
		
		return array(
				"msg" => $message,
				"success" => $success
		);
	}
}