<?php

namespace Home\DAO;

use Home\Service\DataOrgService;
use Home\Common\FIdConst;
use Home\Service\IdGenService;
use Home\Service\UserService;

/**
 * 人员职务 DAO
 *
 * @author 李静波
 */
class UserPositionDAO extends PSIBaseDAO {
	private $LOG_CATEGORY = "人员职务";

	/**
	 * 获得所属公司列表
	 */
	public function getCompanyList($params) {
		$id = $params["id"];
		
		$db = M();
		
		$ds = new DataOrgService();
		$queryParams = array();
		$rs = $ds->buildSQL(FIdConst::USR_MANAGEMENT, "t_org");
		
		$sql = "select id, name 
				from t_org 
				where parent_id is null ";
		if ($rs) {
			$sql .= " and " . $rs[0];
			$queryParams = $rs[1];
		}
		$sql .= " order by org_code";
		
		$data = $db->query($sql, $queryParams);
		$result = array();
		
		foreach ( $data as $i => $v ) {
			$result[$i]["id"] = $v["id"];
			$result[$i]["name"] = $v["name"];
		}
		
		$result = array(
				"companyList" => $result
		);
		if ($id) {
			$sql = "select position_name, company_id 
					from t_user_position
					where id = '%s' ";
			$data = $db->query($sql, $id);
			if ($data) {
				$result["name"] = $data[0]["position_name"];
				$result["companyId"] = $data[0]["company_id"];
			}
		}
		
		return $result;
	}

	/**
	 * 新增或编辑人员职务
	 */
	public function editUserPosition($params) {
		$id = $params["id"];
		$name = $params["name"];
		$companyId = $params["companyId"];
		
		$db = M();
		$db->startTrans();
		$sql = "select count(*) as cnt from t_org where id = '%s' ";
		$data = $db->query($sql, $companyId);
		$cnt = $data[0]["cnt"];
		if ($cnt != 1) {
			$db->rollback();
			return $this->bad("所属公司不存在");
		}
		
		$us = new UserService();
		$dataOrg = $us->getLoginUserDataOrg();
		
		if ($id) {
			// 编辑人员职务
			
			// 检查职务是否存在
			$sql = "select count(*) as cnt from t_user_position
					where position_name = '%s' and company_id = '%s' 
						and id <> '%s' ";
			$data = $db->query($sql, $name, $companyId, $id);
			$cnt = $data[0]["cnt"];
			if ($cnt > 0) {
				$db->rollback();
				return $this->bad("职务[$name]已经存在");
			}
			
			$sql = "update t_user_position
					set position_name = '%s'
					where id = '%s' ";
			$rc = $db->execute($sql, $name, $id);
			if ($rc === false) {
				$db->rollback();
				return $this->sqlError(__LINE__);
			}
			
			$log = "编辑人员职务[$name]";
		} else {
			// 新增人员职务
			
			// 检查职务是否存在
			$sql = "select count(*) as cnt from t_user_position 
					where position_name = '%s' and company_id = '%s' ";
			$data = $db->query($sql, $name, $companyId);
			$cnt = $data[0]["cnt"];
			if ($cnt > 0) {
				$db->rollback();
				return $this->bad("职务[$name]已经存在");
			}
			
			$idGen = new IdGenService();
			$id = $idGen->newId($db);
			
			$sql = "insert into t_user_position (id, position_name, company_id, data_org) 
					values ('%s', '%s', '%s', '%s') ";
			$rc = $db->execute($sql, $id, $name, $companyId, $dataOrg);
			if ($rc === false) {
				$db->rollback();
				return $this->sqlError(__LINE__);
			}
			
			$log = "新增人员职务[$name]";
		}
		
		// 记录业务日志
		if ($log) {
			$bd = new BizlogDAO($db);
			$bd->insertBizlog($log, $this->LOG_CATEGORY);
		}
		
		$db->commit();
		
		return $this->ok($id);
	}

	/**
	 * 获得人员职务列表
	 */
	public function userPositionList() {
		$db = M();
		
		$ds = new DataOrgService();
		$queryParams = array();
		$rs = $ds->buildSQL(FIdConst::USER_POSITION, "u");
		
		$sql = "select u.id, u.position_name, o.name as company_name
				from t_user_position u, t_org o
				where u.company_id = o.id ";
		if ($rs) {
			$sql .= " and " . $rs[0];
			$queryParams = $rs[1];
		}
		$sql .= " order by o.org_code, convert(u.position_name USING gbk) collate gbk_chinese_ci  ";
		$data = $db->query($sql, $queryParams);
		
		$result = array();
		
		foreach ( $data as $i => $v ) {
			$result[$i]["id"] = $v["id"];
			$result[$i]["name"] = $v["position_name"];
			$result[$i]["companyName"] = $v["company_name"];
		}
		
		return $result;
	}

	/**
	 * 删除人员职务
	 */
	public function deleteUserPosition($params) {
		$id = $params["id"];
		
		$db = M();
		$db->startTrans();
		
		// 检查要删除的人员职务是否存在
		$sql = "select u.position_name , o.name
				from t_user_position u, t_org o
				where u.id = '%s' and u.company_id = o.id";
		$data = $db->query($sql, $id);
		if (! $data) {
			$db->rollback();
			return $this->bad("要删除的人员职务不存在");
		}
		
		$positionName = $data[0]["position_name"];
		$companyName = $data[0]["name"];
		
		// 检查人员职务是否被使用了
		$sql = "select count(*) as cnt from t_user
				where position_id = '%s' ";
		$data = $db->query($sql, $id);
		$cnt = $data[0]["cnt"];
		if ($cnt > 0) {
			$db->rollback();
			return $this->bad("要删除的职务[$positionName]已经被使用，不能删除");
		}
		
		$sql = "delete from t_user_position where id = '%s' ";
		$rc = $db->execute($sql, $id);
		if ($rc === false) {
			$db->rollback();
			return $this->sqlError(__LINE__);
		}
		
		$log = "删除公司[$companyName]的职务[$positionName]";
		$bd = new BizlogDAO($db);
		$bd->insertBizlog($log, $this->LOG_CATEGORY);
		
		$db->commit();
		
		return $this->ok();
	}
}