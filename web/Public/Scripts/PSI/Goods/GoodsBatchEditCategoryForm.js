/**
 * 商品 - 批量修改商品分类界面
 */
Ext.define("PSI.Goods.GoodsBatchEditCategoryForm", {
	extend : "Ext.window.Window",

	config : {
		parentForm : null,
		idList : null
		// 数组
	},

	/**
	 * 初始化组件
	 */
	initComponent : function() {
		var me = this;

		var buttons = [];

		buttons.push({
					text : "保存",
					formBind : true,
					iconCls : "PSI-button-ok",
					handler : function() {
						me.onOK(false);
					},
					scope : me
				}, {
					text : "取消",
					handler : function() {
						me.close();
					},
					scope : me
				});

		Ext.apply(me, {
					title : "批量编辑商品分类",
					modal : true,
					resizable : false,
					onEsc : Ext.emptyFn,
					width : 480,
					height : 110,
					layout : "fit",
					items : [{
								id : "editForm",
								xtype : "form",
								layout : {
									type : "table",
									columns : 1
								},
								height : "100%",
								bodyPadding : 5,
								defaultType : 'textfield',
								fieldDefaults : {
									labelWidth : 60,
									labelAlign : "right",
									labelSeparator : "",
									msgTarget : 'side'
								},
								items : [{
											xtype : "hidden",
											name : "idList",
											value : me.getIdList()
										}, {
											id : "editCategory",
											xtype : "psi_goodscategoryfield",
											fieldLabel : "商品分类",
											allowBlank : false,
											blankText : "没有输入商品分类",
											width : 450,
											beforeLabelTextTpl : PSI.Const.REQUIRED,
											listeners : {
												specialkey : {
													fn : me.onEditSpecialKey,
													scope : me
												}
											}
										}, {
											xtype : "hidden",
											name : "categoryId",
											id : "editCategoryId"
										}],
								buttons : buttons
							}],
					listeners : {
						show : {
							fn : me.onWndShow,
							scope : me
						},
						close : {
							fn : me.onWndClose,
							scope : me
						}
					}
				});

		me.callParent(arguments);
	},

	onWndShow : function() {
		var me = this;
	},

	onOK : function(thenAdd) {
		var me = this;

		var categoryId = Ext.getCmp("editCategory").getIdValue();
		Ext.getCmp("editCategoryId").setValue(categoryId);

		var f = Ext.getCmp("editForm");
		var el = f.getEl();
		el.mask(PSI.Const.SAVING);
		f.submit({
					url : PSI.Const.BASE_URL
							+ "Home/Goods/batchEditGoodsCategory",
					method : "POST",
					success : function(form, action) {
						el.unmask();

						PSI.MsgBox.tip("数据保存成功");
						me.focus();

						me.close();
						location.reload();
					},
					failure : function(form, action) {
						el.unmask();
						PSI.MsgBox.showInfo(action.result.msg);
					}
				});
	},

	onEditSpecialKey : function(field, e) {
		if (e.getKey() == e.ENTER) {
			var f = Ext.getCmp("editForm");
			if (f.getForm().isValid()) {
				var me = this;
				me.onOK(me.adding);
			}
		}
	},

	onWndClose : function() {
		var me = this;
	}
});